import javax.swing.table.AbstractTableModel;

import java.util.LinkedList;
import java.util.List;
public class MovieTableModel extends AbstractTableModel {

	private List<Movie> listaFilmow;
	
	public MovieTableModel(){
		super();
		this.listaFilmow = new LinkedList<>();
	}
	
	public void addMovie(Movie m){
		listaFilmow.add(m);
		fireTableDataChanged();
	}
	
	
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return listaFilmow.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
	Movie tmp = listaFilmow.get(rowIndex);
	switch(columnIndex){
	case 0:
		return tmp.getTitle();
	case 1:
		return tmp.getDirector();
	case 2:
		return tmp.getGenre();
	case 3:
		return tmp.getRate();
	case 4:
		return tmp.getYear();
	default:
		return "unknown";
	}
		
	}
	
	

}
