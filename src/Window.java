import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window {

	private JFrame frame;
	private JTextField text;
	private JTextField director;
	private JTextField year;
	private JTextField genre;
	private JTextField rate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */

	private MovieTableModel model;

	public Window() {
		initialize();
		model = new MovieTableModel();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panelLeft = new JPanel();
		panelLeft.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panelLeft);
		GridBagLayout gbl_panelLeft = new GridBagLayout();
		gbl_panelLeft.columnWidths = new int[] { 0, 0 };
		gbl_panelLeft.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panelLeft.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelLeft.rowWeights = new double[] { 1.0, 1.0, 0.0, Double.MIN_VALUE };
		panelLeft.setLayout(gbl_panelLeft);

		JPanel panelLeftTop = new JPanel();
		GridBagConstraints gbc_panelLeftTop = new GridBagConstraints();
		gbc_panelLeftTop.insets = new Insets(0, 0, 5, 0);
		gbc_panelLeftTop.fill = GridBagConstraints.BOTH;
		gbc_panelLeftTop.gridx = 0;
		gbc_panelLeftTop.gridy = 0;
		panelLeft.add(panelLeftTop, gbc_panelLeftTop);
		GridBagLayout gbl_panelLeftTop = new GridBagLayout();
		gbl_panelLeftTop.columnWidths = new int[] { 0 };
		gbl_panelLeftTop.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panelLeftTop.columnWeights = new double[] { 1.0 };
		gbl_panelLeftTop.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
		panelLeftTop.setLayout(gbl_panelLeftTop);

		JLabel lblNewLabel = new JLabel("Dane Filmu");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panelLeftTop.add(lblNewLabel, gbc_lblNewLabel);

		JPanel fieldTitle = new JPanel();
		GridBagConstraints gbc_fieldTitle = new GridBagConstraints();
		gbc_fieldTitle.insets = new Insets(0, 0, 5, 0);
		gbc_fieldTitle.fill = GridBagConstraints.BOTH;
		gbc_fieldTitle.gridx = 0;
		gbc_fieldTitle.gridy = 1;
		panelLeftTop.add(fieldTitle, gbc_fieldTitle);

		JLabel lblNewLabel_1 = new JLabel("Tytu\u0142");
		fieldTitle.add(lblNewLabel_1);

		text = new JTextField();
		text.setHorizontalAlignment(SwingConstants.CENTER);
		fieldTitle.add(text);
		text.setColumns(10);

		JPanel fieldDirector = new JPanel();
		GridBagConstraints gbc_fieldDirector = new GridBagConstraints();
		gbc_fieldDirector.insets = new Insets(0, 0, 5, 0);
		gbc_fieldDirector.fill = GridBagConstraints.BOTH;
		gbc_fieldDirector.gridx = 0;
		gbc_fieldDirector.gridy = 2;
		panelLeftTop.add(fieldDirector, gbc_fieldDirector);

		JLabel lblNewLabel_2 = new JLabel("Re\u017Cyser");
		fieldDirector.add(lblNewLabel_2);

		director = new JTextField();
		director.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldDirector.add(director);
		director.setColumns(10);

		JPanel fieldYear = new JPanel();
		GridBagConstraints gbc_fieldYear = new GridBagConstraints();
		gbc_fieldYear.insets = new Insets(0, 0, 5, 0);
		gbc_fieldYear.fill = GridBagConstraints.BOTH;
		gbc_fieldYear.gridx = 0;
		gbc_fieldYear.gridy = 3;
		panelLeftTop.add(fieldYear, gbc_fieldYear);

		JLabel lblNewLabel_3 = new JLabel("Rok");
		fieldYear.add(lblNewLabel_3);

		year = new JTextField();
		fieldYear.add(year);
		year.setColumns(10);

		JPanel fieldGenre = new JPanel();
		GridBagConstraints gbc_fieldGenre = new GridBagConstraints();
		gbc_fieldGenre.insets = new Insets(0, 0, 5, 0);
		gbc_fieldGenre.fill = GridBagConstraints.BOTH;
		gbc_fieldGenre.gridx = 0;
		gbc_fieldGenre.gridy = 4;
		panelLeftTop.add(fieldGenre, gbc_fieldGenre);

		JLabel lblNewLabel_4 = new JLabel("Gatunek");
		fieldGenre.add(lblNewLabel_4);

		genre = new JTextField();
		fieldGenre.add(genre);
		genre.setColumns(10);

		JPanel fieldRate = new JPanel();
		GridBagConstraints gbc_fieldRate = new GridBagConstraints();
		gbc_fieldRate.fill = GridBagConstraints.BOTH;
		gbc_fieldRate.gridx = 0;
		gbc_fieldRate.gridy = 5;
		panelLeftTop.add(fieldRate, gbc_fieldRate);

		JLabel lblNewLabel_5 = new JLabel("Ocena");
		fieldRate.add(lblNewLabel_5);

		rate = new JTextField();
		fieldRate.add(rate);
		rate.setColumns(10);

		JPanel panelLeftBottom = new JPanel();
		GridBagConstraints gbc_panelLeftBottom = new GridBagConstraints();
		gbc_panelLeftBottom.insets = new Insets(0, 0, 5, 0);
		gbc_panelLeftBottom.fill = GridBagConstraints.BOTH;
		gbc_panelLeftBottom.gridx = 0;
		gbc_panelLeftBottom.gridy = 1;
		panelLeft.add(panelLeftBottom, gbc_panelLeftBottom);
		panelLeftBottom.setLayout(new GridLayout(0, 1, 0, 0));

		JButton add = new JButton("Dodaj");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Movie m = new Movie();

				// ustawienie podstawowych pol
				m.setTitle(text.getText());
				m.setDirector(director.getText());
				m.setYear(year.getText());
				m.setGenre(genre.getText());

				// ustawienie pol konwertowanych (ocena to float)
				m.setRate(Float.parseFloat(rate.getText()));

				model.addMovie(m);
			}
		});
		panelLeftBottom.add(add);

		JButton btnNewButton_2 = new JButton("New button");
		panelLeftBottom.add(btnNewButton_2);

		JButton btnNewButton_1 = new JButton("New button");
		panelLeftBottom.add(btnNewButton_1);

		JButton btnNewButton_3 = new JButton("New button");
		panelLeftBottom.add(btnNewButton_3);

		JPanel panelRight = new JPanel();
		panelRight.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panelRight);
		GridBagLayout gbl_panelRight = new GridBagLayout();
		gbl_panelRight.columnWidths = new int[] { 0, 0 };
		gbl_panelRight.rowHeights = new int[] { 0, 0 };
		gbl_panelRight.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelRight.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panelRight.setLayout(gbl_panelRight);
	}

}
